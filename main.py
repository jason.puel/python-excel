#import libraries
from openpyxl import load_workbook
#=====================================

#Selecting specific sheet
wb = load_workbook("Excel.xlsx")
sheet = wb.worksheets[0] # 0 1 2 3 or any

DataDB = []

def print_sheet(sheet = 0, row_start = 1, column_start = 1):

    sheet = wb.worksheets[sheet] # 0 1 2 3 or any

    line = "|"
    row = row_start
    column = column_start

    while sheet.cell(row = row_start, column = column).value :
        cell = sheet.cell(row = row_start, column = column).value
        column = column + 1
    len_row = column - 1
    while sheet.cell(row = row, column = column_start).value :
        row = row + 1
    len_column = row - 1

    for row in range(row_start,len_column,1):
        for column in range(column_start,len_row,1):
            cell = sheet.cell(row = row, column = column).value
            if cell == None:
                cell = " "
            line = line + '{:10}'.format(cell) + " | "
        print(line)
        line = "|"
        column = row_start


def read_DB(sheet = 1, row_start = 1, column_start = 1):
    sheet = wb.worksheets[sheet] # 0 1 2 3 or any
    row = row_start
    column = column_start

    while sheet.cell(row = row_start, column = column).value :
        column = column + 1
    len_row = column - 1
    while sheet.cell(row = row, column = column_start).value :
        row = row + 1
    len_column = row - 1

    name_of_column = []
    for row in range(row_start,len_column,1):  
        info_line = {}
        for column in range(column_start,len_row,1):
            cell = sheet.cell(row = row, column = column).value
            if row == row_start:
                name_of_column.append(cell)
            else:
                info_line.update({name_of_column[column - 1] : str(cell)})
        if row != row_start: 
            print(info_line)
            DataDB.append(info_line)
            info_line = {}
        column = row_start

#=====================================
#sheet = wb.active # select the currently active

#wb.save("excel.xlsx")

# ================= Main ===================

print_sheet(sheet = 0, row_start = 1, column_start = 1)
print('=============================================')
read_DB()
print('=============================================')
#print_sheet(sheet = 1, row_start = 1, column_start = 1)
print(DataDB)
